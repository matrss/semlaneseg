{
  description = "Semantic segmentation for traffic lane detection";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.devshell.url = "github:numtide/devshell";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.poetry2nix.url = "github:nix-community/poetry2nix";

  outputs = inputs@{ self, ... }: inputs.flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import inputs.nixpkgs { inherit system; };
    in
    {
      packages.doc =
        let
          tex = pkgs.texlive.combine {
            inherit (pkgs.texlive)
              scheme-small
              latexmk
              dvisvgm
              luatex85
              biblatex
              datetime
              fmtcount
              titling
              csquotes
              siunitx
              cleveref
              # standalone and dependencies
              standalone
              currfile
              gincltex
              svn-prov
              adjustbox
              collectbox
              filemod
              ;
          };
        in
        pkgs.stdenvNoCC.mkDerivation rec {
          name = "doc";
          src = ./.;
          buildInputs = [ pkgs.coreutils pkgs.biber tex ];
          phases = [ "unpackPhase" "buildPhase" "installPhase" ];
          buildPhase = ''
            cd doc
            make graphics
            env SOURCE_DATE_EPOCH=$(date -d "2022-01-21" +%s) make pdf
          '';
          installPhase = ''
            mkdir -p $out
            cp -r graphics/ $out/
            cp seminararbeit.pdf $out/
          '';
        };

      packages.slides = pkgs.stdenvNoCC.mkDerivation rec {
        name = "slides";
        src = ./.;
        buildInputs = [ pkgs.pandoc ];
        phases = [ "unpackPhase" "buildPhase" "installPhase" ];
        buildPhase = ''
          cd slides
          cp -r ${self.packages.${system}.doc}/graphics/generated/ static/graphics/generated/
          make slides
        '';

        installPhase = ''
          mkdir -p $out
          cp -r -L static $out/
          cp slides.html $out/
        '';
      };

      packages.semlaneseg =
        let
          pkgs = import inputs.nixpkgs { inherit system; overlays = [ inputs.poetry2nix.overlay ]; };
        in
        pkgs.poetry2nix.mkPoetryApplication {
          projectDir = ./.;
        };

      devShell = pkgs.mkShell {
        name = "semlaneseg";
        inputsFrom = [ self.packages.${system}.doc self.packages.${system}.slides ];
        buildInputs = with pkgs; [ poetry ffmpeg-full graphviz ];
      };
    });
}
