% vim: filetype=tex

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{seminararbeit}

\LoadClass[ngerman,
           parskip=half,
           fontsize=12pt,
           toc=bibliography,
          ]{scrreprt}

\RequirePackage{titling}
\RequirePackage[pass]{geometry}

\newcommand{\seminararbeit@matrikelnummer}{}
\newcommand{\seminararbeitMatrikelnummer}[1]{\renewcommand{\seminararbeit@matrikelnummer}{#1}}

\renewcommand\maketitle{%
  \begin{titlepage}
    \thispagestyle{empty}
    \newgeometry{left=0cm, right=0cm, top=0.6cm, bottom=0cm}
    \begin{flushright}
      \includegraphics[width=1.7cm]{./graphics/FHAC}
    \end{flushright}
    \vspace{-2.5cm}
    \begin{center}
      \begin{minipage}{\textwidth}
        \centering
        \textbf{\large Fachhochschule Aachen}\\
        Campus Jülich\\
        Fachbereich 9 -- Medizintechnik und Technomathematik\\
        \vspace{1cm}
        Studiengang: Angewandte Mathematik und Informatik\\
	Matrikelnummer: \seminararbeit@matrikelnummer{}\\
      \end{minipage}

      \vspace{\fill}

      \begin{minipage}{0.75\textwidth}
        \centering \bfseries \Large
        \thetitle
      \end{minipage}

      \vspace{\fill}

      \begin{minipage}{\textwidth}
        \centering
        Seminararbeit von\\[5mm]
        {\bfseries\large\theauthor}\\[5mm]
        \thedate
      \end{minipage}

      \vspace{\fill}
    \end{center}
  \end{titlepage}
}
