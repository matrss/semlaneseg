# import numpy as np
import tensorflow as tf


def create_model(input_size, num_classes, batch_size=None):
    def conv_block(filters, kernel_size=3, activation="relu"):
        def _conv_block(inputs):
            out = tf.keras.layers.Conv2D(filters, kernel_size, padding="same")(inputs)
            out = tf.keras.layers.BatchNormalization()(out)
            out = tf.keras.layers.Activation(activation)(out)
            out = tf.keras.layers.Conv2D(filters, kernel_size, padding="same")(out)
            out = tf.keras.layers.BatchNormalization()(out)
            out = tf.keras.layers.Activation(activation)(out)
            return out

        return _conv_block

    def encode(inputs):
        conv1 = conv_block(32)(inputs)
        pool1 = tf.keras.layers.MaxPool2D((2, 2))(conv1)

        conv2 = conv_block(64)(pool1)
        pool2 = tf.keras.layers.MaxPool2D((2, 2))(conv2)

        conv3 = conv_block(128)(pool2)
        pool3 = tf.keras.layers.MaxPool2D((2, 2))(conv3)

        conv4 = conv_block(256)(pool3)
        pool4 = tf.keras.layers.MaxPool2D((2, 2))(conv4)

        conv5 = conv_block(512)(pool4)

        return conv1, conv2, conv3, conv4, conv5

    def decode(conv5, conv4, conv3, conv2, conv1, num_classes):
        up5 = tf.keras.layers.Conv2DTranspose(256, 2, strides=(2, 2), padding="same")(
            conv5
        )
        concat6 = tf.keras.layers.concatenate([up5, conv4], axis=3)
        conv6 = conv_block(256)(concat6)

        up6 = tf.keras.layers.Conv2DTranspose(128, 2, strides=(2, 2), padding="same")(
            conv6
        )
        concat7 = tf.keras.layers.concatenate([up6, conv3], axis=3)
        conv7 = conv_block(128)(concat7)

        up7 = tf.keras.layers.Conv2DTranspose(64, 2, strides=(2, 2), padding="same")(
            conv7
        )
        concat8 = tf.keras.layers.concatenate([up7, conv2], axis=3)
        conv8 = conv_block(64)(concat8)

        up8 = tf.keras.layers.Conv2DTranspose(32, 2, strides=(2, 2), padding="same")(
            conv8
        )
        concat9 = tf.keras.layers.concatenate([up8, conv1], axis=3)
        conv9 = conv_block(32)(concat9)

        conv10 = tf.keras.layers.Conv2D(num_classes, 1, padding="same")(conv9)
        out = tf.keras.layers.Softmax(axis=-1)(conv10)

        return out

    inputs = tf.keras.layers.Input(input_size, batch_size=batch_size)
    conv1, conv2, conv3, conv4, conv5 = encode(inputs)
    outputs = decode(conv5, conv4, conv3, conv2, conv1, num_classes)
    model = tf.keras.models.Model(inputs, outputs)
    return model
