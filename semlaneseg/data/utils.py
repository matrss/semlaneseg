import json

import tensorflow as tf


def decode_json(record_str):
    def _decode_json(record_str):
        record_dict = json.loads(record_str.numpy())
        return record_dict["image"], record_dict["mask"]

    return tf.py_function(_decode_json, [record_str], [tf.string, tf.string])


def train_val_split(
    dataset,
    dataset_size,
    train_ratio=0.8,
    val_ratio=0.2,
    shuffle=True,
    shuffle_seed=None,
):
    """Splits a tensorflow dataset into training and validation datasets.

    :param dataset: tensorflow dataset
    :param dataset_size: size of the dataset
    :param train_ratio: ratio of training data
    :param val_ratio: ratio of validation data
    :param shuffle: if the dataset should be shuffled first
    :param shuffle_seed: seed for the shuffle
    :returns: (train_ds, val_ds) tuple
    """
    if shuffle:
        dataset.shuffle(dataset_size, seed=shuffle_seed)

    train_size = int(train_ratio * dataset_size)
    val_size = int(val_ratio * dataset_size)

    train_ds = dataset.take(train_size)
    val_ds = dataset.skip(train_size).take(val_size)

    return train_ds, val_ds
