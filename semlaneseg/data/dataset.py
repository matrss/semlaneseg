import numpy as np
import tensorflow as tf

import semlaneseg.image
from semlaneseg.data.utils import decode_json, train_val_split


def read_files(base_path):
    def _read_files(image_path, mask_path):
        image_path = tf.strings.join([base_path, image_path], separator="/")
        mask_path = tf.strings.join([base_path, mask_path], separator="/")
        image = tf.io.read_file(image_path)
        image = tf.image.decode_jpeg(image)
        image = tf.image.convert_image_dtype(image, tf.float32)
        image.set_shape((144, 256, 3))

        def _load_mask(mask_path):
            return np.load(mask_path.numpy())

        mask = tf.py_function(_load_mask, [mask_path], tf.uint8)
        mask.set_shape((144, 256))
        mask = tf.one_hot(mask, 7)

        return image, mask

    return _read_files


def augment_image_color_and_quality(element, seed):
    image, mask = element

    shape = image.shape

    seeds = tf.random.experimental.stateless_split(seed, num=5)

    image = tf.image.stateless_random_hue(image, 0.2, seeds[0])
    image = tf.image.stateless_random_saturation(image, 0.5, 1.0, seeds[1])
    image = tf.image.stateless_random_brightness(image, 0.2, seeds[2])
    image = tf.image.stateless_random_contrast(image, 0.2, 0.5, seeds[3])
    image = tf.image.stateless_random_jpeg_quality(image, 25, 100, seeds[4])

    image.set_shape(shape)

    return image, mask


def augment_image_rotation_and_cutout(element, seed):
    image, mask = element

    image_height, image_width, _ = image.shape

    seeds = tf.random.experimental.stateless_split(seed, num=2)

    image, mask = semlaneseg.image.stateless_random_rotation(
        image, mask, [0.1, 0.1, 10], seeds[0]
    )
    image = semlaneseg.image.stateless_random_cutout(
        image,
        (image_height // 4, image_width // 4),
        (image_height // 2, image_width // 2),
        seeds[1],
    )

    return image, mask


def get_train_val_datasets(records_path, seed, augmentations=None):
    if augmentations is None:
        augmentations = {}

    rng = tf.random.Generator.from_seed(seed)

    def _augment(image, mask):
        if "color_and_quality" in augmentations:
            seed = rng.make_seeds(2)[0]
            image, mask = augment_image_color_and_quality((image, mask), seed)
        if "rotation_and_cutout" in augmentations:
            seed = rng.make_seeds(2)[0]
            image, mask = augment_image_rotation_and_cutout((image, mask), seed)
        return image, mask

    record_ds = tf.data.TextLineDataset(records_path).map(
        decode_json, num_parallel_calls=tf.data.AUTOTUNE
    )
    train_ds, val_ds = train_val_split(
        record_ds, len(list(record_ds)), shuffle_seed=rng.make_seeds(1)[0, 0]
    )

    train_ds = train_ds.map(
        read_files("data/processed/tusimple/train_set"),
        num_parallel_calls=tf.data.AUTOTUNE,
    ).map(_augment, num_parallel_calls=tf.data.AUTOTUNE)

    val_ds = val_ds.map(
        read_files("data/processed/tusimple/train_set"),
        num_parallel_calls=tf.data.AUTOTUNE,
    )

    return train_ds, val_ds


def get_test_dataset(records_path):
    record_ds = tf.data.TextLineDataset(records_path).map(
        decode_json, num_parallel_calls=tf.data.AUTOTUNE
    )
    test_ds = record_ds.map(
        read_files("data/processed/tusimple/test_set"),
        num_parallel_calls=tf.data.AUTOTUNE,
    )
    return test_ds
