import tensorflow as tf
import tensorflow_graphics as tfg
import tensorflow_graphics.geometry.transformation
import tensorflow_graphics.image.transformer


def stateless_random_cutout(image, min_size, max_size, seed, fill_value=0):
    """Replaces a random rectangle in each image with fill_value."""

    image_height, image_width, _ = image.shape

    seeds = tf.random.experimental.stateless_split(seed, num=4)

    # Generate center position and size of masks
    y = tf.random.stateless_uniform(
        [1], seed=seeds[0], minval=[0], maxval=[image_height]
    )
    x = tf.random.stateless_uniform(
        [1], seed=seeds[1], minval=[0], maxval=[image_width]
    )
    height = tf.random.stateless_uniform(
        [1], seed=seeds[2], minval=[min_size[0]], maxval=[max_size[0]]
    )
    width = tf.random.stateless_uniform(
        [1], seed=seeds[3], minval=[min_size[1]], maxval=[max_size[1]]
    )

    y, x = y - height / 2, x - width / 2
    y, x, height, width = map(lambda t: tf.cast(t, tf.int32)[0], [y, x, height, width])

    image = tf.convert_to_tensor(image)
    lower_bound = tf.maximum(0, y)
    upper_bound = tf.minimum(image_height, y + height)
    left_bound = tf.maximum(0, x)
    right_bound = tf.minimum(image_width, x + width)

    mask = tf.pad(
        tf.zeros((upper_bound - lower_bound, right_bound - left_bound), dtype=tf.bool),
        [
            [lower_bound, image_height - upper_bound],
            [left_bound, image_width - right_bound],
        ],
        constant_values=True,
    )
    mask = tf.expand_dims(mask, axis=-1)

    image = tf.where(
        mask,
        image,
        tf.cast(fill_value, dtype=image.dtype),
    )

    return image


def stateless_random_rotation(image, mask, max_angles, seed):
    """Applies a random 3-dimensional rotation to each image."""

    image_height, image_width, _ = image.shape

    # Translation matrices to rotate around the center point of the images
    B = tf.constant(
        [[[1.0, 0.0, -image_width / 2], [0.0, 1.0, -image_height / 2], [0.0, 0.0, 1.0]]]
    )
    C = tf.constant(
        [[[1.0, 0.0, image_width / 2], [0.0, 1.0, image_height / 2], [0.0, 0.0, 1.0]]]
    )

    max_angles = tf.convert_to_tensor(max_angles)

    # Generate random rotation angles
    angles = tf.random.stateless_uniform(
        [1, 3], seed=seed, minval=-max_angles, maxval=max_angles
    )
    angles = tf.experimental.numpy.deg2rad(angles)

    # Build rotation matrix from angles
    A = tfg.geometry.transformation.rotation_matrix_3d.from_euler(angles)
    A = tf.cast(A, tf.float32)

    image = tfg.image.transformer.perspective_transform(
        tf.expand_dims(image, axis=0), C @ A @ B
    )[0]
    mask = tfg.image.transformer.perspective_transform(
        tf.expand_dims(mask, axis=0),
        C @ A @ B,
        resampling_type=tfg.image.transformer.ResamplingType.NEAREST,
        border_type=tfg.image.transformer.BorderType.DUPLICATE,
    )[0]

    return image, mask
