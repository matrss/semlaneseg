import json
import pathlib
import tarfile
import tempfile

import holoviews as hv
import numpy as np
import tensorflow as tf


def save_model_and_history(model, history, path):
    """Save model and training history together.

    The model is saved using the tensorflow `saved_model` builtin utilities;
    from the history object only `epoch`, `params` and `history` are serialized
    to json.
    The resulting `saved_model` directory and `history.json` file are combined into
    a `tar.gz` file.

    :param model: keras model
    :param history: history object as returned by :py:func:`model.fit`
    :param path: path to save to
    """
    with tempfile.TemporaryDirectory() as td:
        td = pathlib.Path(td)
        model_path = td / "model"
        history_path = td / "history.json"
        model.save(model_path)
        with open(history_path, mode="w") as f:
            json.dump(
                {
                    "dataset_args": history.dataset_args,
                    "epoch": history.epoch,
                    "params": history.params,
                    "history": history.history,
                },
                f,
            )
        with tarfile.open(path, mode="w:gz") as f:
            f.add(model_path, arcname="model")
            f.add(history_path, arcname="history.json")


def load_model_and_history(path):
    """Load model and history from path.

    This reads a savefile created by :py:func:`save_model_and_history`.

    :param path: path to load from
    :returns: (model, history) tuple
    """
    with tempfile.TemporaryDirectory() as td:
        td = pathlib.Path(td)
        model_path = td / "model"
        history_path = td / "history.json"
        with tarfile.open(path, mode="r:gz") as f:
            f.extractall(td)
        model = tf.keras.models.load_model(model_path)
        with open(history_path, mode="r") as f:
            history = json.load(f)
    return model, history


def image_path_to_mask_path(image_path, mask_dir, shape):
    return (
        pathlib.Path(mask_dir)
        / "{}x{}".format(*shape)
        / str(image_path).removesuffix(".jpg")
    )


def plot_segmentations(mask, num_classes):
    return hv.Layout([hv.Image(mask == label) for label in range(num_classes)])


def mask_to_image(mask):
    """Converts a mask into a RGB image."""
    colors = [
        [0, 0, 0],
        [255, 0, 0],
        [0, 255, 0],
        [0, 0, 255],
        [255, 0, 255],
        [0, 255, 255],
        [255, 255, 255],
    ]
    out = np.zeros((*mask.shape, 3), dtype=np.uint8)
    for i in np.unique(mask):
        out[mask == i] = colors[i]
    return out
