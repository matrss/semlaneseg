import argparse
import pathlib

import skimage as ski
import skimage.io
import skimage.transform
import tqdm


def run(input_dir, out_dir, shape):
    input_dir = pathlib.Path(input_dir)
    out_dir = pathlib.Path(out_dir)

    print("Processing directory {}...".format(input_dir))
    for image_path in tqdm.tqdm(list(input_dir.glob("**/20.jpg"))):
        image = ski.io.imread(image_path)
        image = ski.transform.resize(image, shape, anti_aliasing=True)
        image = ski.img_as_ubyte(image)
        rel_path = image_path.relative_to(input_dir)
        out_path = (out_dir / "{}x{}".format(*shape) / rel_path).resolve()
        out_path.parent.mkdir(parents=True, exist_ok=True)
        ski.io.imsave(out_path, image)


def main():
    parser = argparse.ArgumentParser(description="Resize images.")
    parser.add_argument(
        "input_dir",
        metavar="PATH",
        type=pathlib.Path,
        help="path to a directory to search for jpg files in",
    )
    parser.add_argument(
        "-o",
        "--out_dir",
        metavar="PATH",
        type=pathlib.Path,
        required=True,
        help="directory to write to",
    )
    parser.add_argument(
        "-s",
        "--shape",
        metavar="HxW",
        type=lambda x: tuple(map(int, x.split("x"))),
        required=True,
        help="shape to resize to",
    )
    args = parser.parse_args()
    run(**vars(args))


if __name__ == "__main__":
    main()
