import argparse
import pathlib

import tensorflow as tf

import semlaneseg.data.dataset
import semlaneseg.utils


def representative_data_gen(*args, **kwargs):
    def _data_gen():
        train_ds, _ = semlaneseg.data.dataset.get_train_val_datasets(
            *args, **kwargs, seed=3027282868857919671
        )
        for image, _ in train_ds.batch(1).take(100):
            yield [image]

    return _data_gen


def run(model_path, save_path):
    tf.random.set_seed(1189709468334809077)

    model_path = pathlib.Path(model_path)
    save_path = pathlib.Path(save_path)

    _model, history = semlaneseg.utils.load_model_and_history(model_path)
    # Set a fixed batch size of 1, which is required for the tpu compiler
    _model.input.set_shape((1, *_model.input.get_shape()[1:]))
    # Remove softmax layer, as it is not necessary for inference
    # outputs = _model.layers[-2].output
    # Add an argmax layer to get segmentation classes right out of the model
    # outputs = tf.math.argmax(outputs, axis=-1)

    outputs = _model.layers[-1].output
    model = tf.keras.Model(_model.input, outputs)

    converter = tf.lite.TFLiteConverter.from_keras_model(model)
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    converter.representative_dataset = representative_data_gen(
        **history["dataset_args"]
    )
    converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
    converter.target_spec.supported_types = [tf.int8]
    converter.inference_input_type = tf.uint8
    converter.inference_output_type = tf.uint8
    tflite_model = converter.convert()

    save_path.parent.mkdir(parents=True, exist_ok=True)
    with open(save_path, mode="wb") as tflite_model_file:
        tflite_model_file.write(tflite_model)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "model_path",
        metavar="IN_PATH",
        type=pathlib.Path,
        help="Path to saved model",
    )
    parser.add_argument(
        "save_path",
        metavar="OUT_PATH",
        type=pathlib.Path,
        help="Path to save quantized tflite model at",
    )
    args = parser.parse_args()
    run(**vars(args))


if __name__ == "__main__":
    main()
