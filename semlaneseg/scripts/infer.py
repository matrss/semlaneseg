import argparse
import pathlib

import ffmpeg
import numpy as np
from PIL import Image, ImageDraw
from pycoral.adapters import common, segment
from pycoral.utils.edgetpu import make_interpreter

import semlaneseg.utils


def segment_lanes(interpreter, frame):
    common.set_input(interpreter, frame)
    interpreter.invoke()
    result = segment.get_output(interpreter)
    if len(result.shape) == 3:
        result = np.argmax(result, axis=-1)
    return result.astype(np.uint8)


def get_predictor(model_type, model_path):
    if model_type == "tf":
        model, _ = semlaneseg.utils.load_model_and_history(model_path)
        h, w = model.input_shape[1:3]

        def _predict(frame):
            frame = np.array(frame)
            frame = frame[None] / 255.0
            result = model.predict(frame)[0]
            if len(result.shape) == 3:
                result = np.argmax(result, axis=-1)
            return result.astype(np.uint8)

        return _predict, (w, h)
    if model_type == "tflite":
        interpreter = make_interpreter(str(model_path))
        interpreter.allocate_tensors()
        w, h = common.input_size(interpreter)
        return lambda frame: segment_lanes(interpreter, frame), (w, h)
    if model_type == "tflite-tpu":
        interpreter = make_interpreter(str(model_path))
        interpreter.allocate_tensors()
        w, h = common.input_size(interpreter)
        return lambda frame: segment_lanes(interpreter, frame), (w, h)


def run(
    model_type,
    model_path,
    input_path,
    shape,
    stay_in_lane=False,
    left_lane_points=None,
    right_lane_points=None,
    out_path=None,
    out_framerate=None,
):
    height, width = shape

    predictor, (model_width, model_height) = get_predictor(model_type, model_path)

    if stay_in_lane:
        (lx1, ly1), (lx2, ly2) = left_lane_points
        (lx1, ly1), (lx2, ly2) = (
            lx1 / width * model_width,
            ly1 / height * model_height,
        ), (lx2 / width * model_width, ly2 / height * model_height)

        def left_lane(y):
            a = (ly2 - ly1) / (lx2 - lx1)
            b = ly1 - lx1 * a
            return (y - b) / a

        left_lane_vals = left_lane(np.arange(0, 144))

        (rx1, ry1), (rx2, ry2) = right_lane_points
        (rx1, ry1), (rx2, ry2) = (
            rx1 / width * model_width,
            ry1 / height * model_height,
        ), (rx2 / width * model_width, ry2 / height * model_height)

        def right_lane(y):
            a = (ry2 - ry1) / (rx2 - rx1)
            b = ry1 - rx1 * a
            return (y - b) / a

        right_lane_vals = right_lane(np.arange(0, 144))

    ffmpeg_input = (
        ffmpeg.input(input_path, re=None)
        .output("pipe:", format="rawvideo", pix_fmt="rgb24")
        .run_async(pipe_stdout=True)
    )

    ffmpeg_output = ffmpeg.input(
        "pipe:", format="rawvideo", pix_fmt="rgb24", s="{}x{}".format(width, height)
    )
    screen_output = ffmpeg_output.output("out", f="sdl2", pix_fmt="rgb24")
    if out_path is not None and out_framerate is not None:
        ffmpeg_output = ffmpeg.merge_outputs(
            screen_output,
            ffmpeg_output.filter("fps", fps=out_framerate)
            .output(str(out_path), pix_fmt="yuvj420p")
            .overwrite_output(),
        )
    else:
        ffmpeg_output = screen_output
    ffmpeg_output = ffmpeg_output.run_async(pipe_stdin=True, quiet=True)

    while True:
        in_bytes = ffmpeg_input.stdout.read(width * height * 3)
        if not in_bytes:
            break
        frame = Image.frombytes("RGB", (width, height), in_bytes)
        frame_resized = frame.resize((model_width, model_height), Image.ANTIALIAS)
        mask = predictor(frame_resized)
        if stay_in_lane:
            d = ImageDraw.Draw(frame)
            d.line(left_lane_points, fill=(0, 255, 0))
            d.line(right_lane_points, fill=(255, 0, 0))

            # Get the average distance of the segmentation of the left lane marking to
            # the expected lane marking as provided on the commandline.
            # The deltas are scaled with the model input width.
            # The segmented lane marking is approximated by the left most pixel of its segmentation.
            left_indices = (mask == 1).argmax(axis=1)
            # left_indices = np.nanmedian(np.where(mask == 1, np.arange(model_width), np.nan), axis=1)
            left_diff = (left_lane_vals - left_indices)[
                left_indices
                != 0
                # np.logical_not(np.isnan(left_indices))
            ]
            left_diff_acc = np.mean(left_diff / model_width)
            left_diff_acc = np.nan_to_num(left_diff_acc)

            # Same as above, but for the right lane marking.
            # The segmented lane marking is approximated by the right most pixel of its segmentation.
            right_indices = 255 - (mask == 2)[:, ::-1].argmax(axis=1)
            # right_indices = np.nanmedian(np.where(mask == 2, np.arange(model_width), np.nan), axis=1)
            right_diff = (right_lane_vals - right_indices)[
                right_indices
                != 255
                # np.logical_not(np.isnan(right_indices))
            ]
            right_diff_acc = np.mean(right_diff / model_width)
            right_diff_acc = np.nan_to_num(right_diff_acc)

            # Create an indicator of where in our current lane we are.
            # We take the mean of the left and right indicators from above.
            # The result is roughly:
            # "how far do the segmented lane markings deviate from the 'true' markings
            # relative to the image width?"
            indicator = (np.abs(left_diff_acc) + np.abs(right_diff_acc)) / 2

            bigger_diff = (
                left_diff_acc
                if np.abs(left_diff_acc) > np.abs(right_diff_acc)
                else right_diff_acc
            )
            direction = "left" if np.sign(bigger_diff) == -1 else "right"

            # With a threshold of 5% this indicator seems to work pretty well.
            if indicator > 0.05:
                if direction == "left":
                    d.polygon(
                        (
                            (width / 3, height / 2),
                            (2 * width / 3, height / 3),
                            (2 * width / 3, 2 * height / 3),
                        ),
                        outline=(255, 255, 255),
                    )
                elif direction == "right":
                    d.polygon(
                        (
                            (2 * width / 3, height / 2),
                            (width / 3, 2 * height / 3),
                            (width / 3, height / 3),
                        ),
                        outline=(255, 255, 255),
                    )
            if indicator <= 0.05:
                d.polygon(
                    (
                        (width / 2, height / 4),
                        (3 * width / 5, 3 * height / 4),
                        (2 * width / 5, 3 * height / 4),
                    ),
                    outline=(255, 255, 255),
                )

        mask = Image.fromarray(mask)
        mask = mask.resize((width, height), Image.NEAREST)
        mask = Image.fromarray(semlaneseg.utils.mask_to_image(np.array(mask)))
        out = Image.blend(frame, mask, 0.5)
        ffmpeg_output.stdin.write(out.tobytes())

    ffmpeg_output.stdin.close()
    ffmpeg_input.wait()
    ffmpeg_output.wait()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input_path",
        metavar="PATH",
        type=pathlib.Path,
        help="Path to video file",
    )
    parser.add_argument(
        "-s",
        "--shape",
        metavar="HxW",
        type=lambda x: tuple(map(int, x.split("x"))),
        required=True,
        help="Shape of the input video",
    )
    parser.add_argument(
        "-t",
        "--model_type",
        choices=("tf", "tflite", "tflite-tpu"),
        required=True,
        help="Specifies the type of model to be used",
    )
    parser.add_argument(
        "-m",
        "--model_path",
        type=pathlib.Path,
        required=True,
        help="Path to the model to use, filetype depends on the type option",
    )
    parser.add_argument(
        "-o",
        "--out_path",
        metavar="PATH",
        type=pathlib.Path,
        help="Path to save the segmentation to as a video file",
    )
    parser.add_argument(
        "-r",
        "--out_framerate",
        metavar="N",
        type=int,
        help="Framerate of the target video file",
    )
    parser.add_argument(
        "--stay_in_lane",
        action="store_true",
        help="Applies a simple algorithm to check if the car is in it's lane",
    )
    parser.add_argument(
        "--left_lane_points",
        metavar="x1,y1;x2,y2",
        type=lambda x: tuple(
            map(lambda e: tuple(map(int, e.split(","))), x.split(";"))
        ),
        help="Start and end point of a line approximating where the left lane marking is expected in the video",
    )
    parser.add_argument(
        "--right_lane_points",
        metavar="x1,y1;x2,y2",
        type=lambda x: tuple(
            map(lambda e: tuple(map(int, e.split(","))), x.split(";"))
        ),
        help="Start and end point of a line approximating where the right lane marking is expected in the video",
    )
    args = parser.parse_args()
    if args.out_path is not None and args.out_framerate is None:
        parser.error("--out_path requires --out_framerate.")
    if args.stay_in_lane and (
        args.left_lane_points is None or args.right_lane_points is None
    ):
        parser.error(
            "--stay_in_lane requires --left_lane_points and --right_lane_points"
        )
    run(**vars(args))


if __name__ == "__main__":
    main()
