import os

os.environ["TF_XLA_FLAGS"] = "--tf_xla_enable_xla_devices"

import argparse
import pathlib

import tensorflow as tf

import semlaneseg.data.dataset
import semlaneseg.models
import semlaneseg.utils


def run(records_path, save_path, shape, batch_size, augmentations):
    tf.random.set_seed(1189709468334809077)

    num_classes = 7

    print(augmentations)

    train_ds, val_ds = semlaneseg.data.dataset.get_train_val_datasets(
        records_path, seed=3027282868857919671, augmentations=augmentations
    )
    train_ds, val_ds = map(
        lambda x: x.batch(batch_size).prefetch(tf.data.AUTOTUNE), (train_ds, val_ds)
    )

    model = semlaneseg.models.create_model((*shape, 3), num_classes)
    model.compile(
        optimizer=tf.keras.optimizers.Adam(),
        loss=tf.keras.losses.CategoricalCrossentropy(),
        metrics=[
            tf.keras.metrics.OneHotMeanIoU(num_classes, name="mean_iou"),
            *[
                tf.keras.metrics.OneHotIoU(num_classes, [i], name="iou_{}".format(i))
                for i in range(num_classes)
            ],
        ],
    )
    print(model.summary())

    history = model.fit(
        train_ds,
        validation_data=val_ds,
        epochs=10000,
        callbacks=[
            tf.keras.callbacks.EarlyStopping(
                monitor="val_loss", patience=10, restore_best_weights=True
            ),
        ],
        verbose=2,
    )
    history.dataset_args = {
        "records_path": str(records_path),
        "augmentations": augmentations,
    }

    semlaneseg.utils.save_model_and_history(model, history, save_path)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "records_path",
        metavar="RECORDS_PATH",
        type=pathlib.Path,
        help="Path to the training records to use",
    )
    parser.add_argument(
        "save_path",
        metavar="OUT_PATH",
        type=pathlib.Path,
        help="Path to save the model to",
    )
    parser.add_argument(
        "-s",
        "--shape",
        metavar="HxW",
        type=lambda x: tuple(map(int, x.split("x"))),
        required=True,
        help="Shape of the data to use",
    )
    parser.add_argument(
        "-b",
        "--batch_size",
        metavar="N",
        type=int,
        required=True,
        help="Batch size to use",
    )
    parser.add_argument(
        "-a",
        "--augmentations",
        action="append",
        choices=["color_and_quality", "rotation_and_cutout"],
        help="Determines the image augmentations to be used, can be specified multiple times",
    )
    args = parser.parse_args()
    run(**vars(args))


if __name__ == "__main__":
    main()
