import argparse
import pathlib

import numpy as np
import skimage as ski
import skimage.io

import semlaneseg.utils


def run(mask_path, out_path=None):
    mask = np.load(mask_path)
    out = semlaneseg.utils.mask_to_image(mask)
    if out_path:
        ski.io.imsave(out_path, out)
    else:
        ski.io.imshow(out)
        ski.io.show()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "mask_path",
        metavar="PATH",
        type=pathlib.Path,
        help="Path to mask file",
    )
    parser.add_argument(
        "-o",
        "--out_path",
        metavar="PATH",
        type=pathlib.Path,
        help="Path to save the mask as an image at",
    )
    args = parser.parse_args()
    run(**vars(args))


if __name__ == "__main__":
    main()
