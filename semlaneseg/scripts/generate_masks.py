import argparse
import json
import pathlib

import numpy as np
import skimage as ski
import skimage.draw
import skimage.morphology
import tqdm

import semlaneseg.utils

image_shape = (720, 1280)


def label_to_mask(label, target_shape):
    lanes = np.zeros(target_shape, dtype="uint8")
    y = np.rint(np.r_[label["h_samples"]] * (target_shape[0] / image_shape[0])).astype(
        "int"
    )

    # Return empty mask early if there are no lanes
    if len(label["lanes"]) == 0:
        return lanes

    # Some labels contain completely empty lanes (all values -2), filter those out
    label["lanes"] = list(filter(lambda l: not all(e == -2 for e in l), label["lanes"]))

    # We sort the lane markings by how low in the image they start. Then we take the
    # first two of those and sort them horizontally at a location where both exist.
    # We can then assume that those are ego-lane left and ego-lane right respectively.
    # Then we iterate over the remaining lane markings in the vertical sort order.
    # If a lane marking is left of the left-most lane up to this point, then we assume
    # it is the next one on the left. The same happens on the right side.
    sorted_lanes = []
    label_lanes = np.array(label["lanes"])
    vert_sort = (label_lanes == -2)[:, ::-1].argmin(axis=1).argsort()
    ego_left_right = label_lanes[vert_sort[0:2]]
    ego_left_right = ego_left_right[
        ego_left_right[:, np.all(ego_left_right != -2, axis=0).argmax()].argsort()
    ]

    left_most = ego_left_right[0]
    right_most = ego_left_right[1]

    left_idx = 1
    right_idx = 2
    sorted_lanes.append((left_idx, ego_left_right[0]))
    sorted_lanes.append((right_idx, ego_left_right[1]))

    for i in vert_sort[2:]:
        lane = label_lanes[i]
        mask = np.logical_and(lane != -2, left_most != -2)
        if np.all(lane[mask] <= left_most[mask]):
            left_idx += 2
            sorted_lanes.append((left_idx, lane))
            left_most = lane
        mask = np.logical_and(lane != -2, right_most != -2)
        if np.all(lane[mask] >= right_most[mask]):
            right_idx += 2
            sorted_lanes.append((right_idx, lane))
            right_most = lane

    # Now we just iterate over all the (number, lane) tuples and draw them into the
    # output mask.
    for i, lane in sorted_lanes:
        x = np.r_[lane]
        mask = x != -2
        x_masked = np.rint(x[mask] * (target_shape[1] / image_shape[1])).astype("int")
        y_masked = y[mask]
        polygon = np.c_[y_masked, x_masked]
        if len(polygon):
            for start, end in zip(polygon[:-2], polygon[1:]):
                rr, cc = ski.draw.line(*start, *end)
                lanes[rr, cc] = i
    lanes = ski.morphology.dilation(lanes)
    return lanes


def load_labels(labels_path):
    with open(labels_path) as f:
        return [json.loads(line) for line in f]


def run(labels_paths, shape, out_dir):
    for labels_path in labels_paths:
        print("Loading labels from {}...".format(labels_path))
        labels = load_labels(labels_path)
        for label in tqdm.tqdm(labels):
            mask = label_to_mask(label, target_shape=shape)
            mask_rel_path = pathlib.Path(*pathlib.Path(label["raw_file"]).parts[1:])
            mask_path = semlaneseg.utils.image_path_to_mask_path(
                mask_rel_path, out_dir, shape
            ).resolve()
            mask_path.parent.mkdir(parents=True, exist_ok=True)
            np.save(mask_path, mask)


def main():
    parser = argparse.ArgumentParser(
        description="Generate target masks from tusimple label data."
    )
    parser.add_argument(
        "labels_paths",
        metavar="PATH",
        type=pathlib.Path,
        nargs="+",
        help="path to a tusimple label json file",
    )
    parser.add_argument(
        "-o",
        "--out_dir",
        metavar="PATH",
        type=pathlib.Path,
        required=True,
        help="directory to write the mask files to",
    )
    parser.add_argument(
        "-s",
        "--shape",
        metavar="HxW",
        type=lambda x: tuple(map(int, x.split("x"))),
        required=True,
        help="shape to generate the masks in",
    )
    args = parser.parse_args()
    run(**vars(args))


if __name__ == "__main__":
    main()
