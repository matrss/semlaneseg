import argparse
import json
import pathlib

import tqdm


def run(input_path, output_path, shape):
    input_path = pathlib.Path(input_path)
    shape_str = "{}x{}".format(*shape)
    images_path = input_path / "clips" / shape_str
    masks_path = input_path / "masks" / shape_str
    with open(output_path, mode="w") as f:
        for image_path in tqdm.tqdm(list(images_path.glob("**/20.jpg"))):
            relative_image_path = image_path.relative_to(images_path)
            mask_path = masks_path / relative_image_path.with_suffix(".npy")
            f.write(
                json.dumps(
                    {
                        "image": str(image_path.relative_to(input_path)),
                        "mask": str(mask_path.relative_to(input_path)),
                    }
                )
            )
            f.write("\n")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "input_path",
        metavar="IN_PATH",
        type=pathlib.Path,
        help="path to processed data",
    )
    parser.add_argument(
        "output_path",
        metavar="OUT_PATH",
        type=pathlib.Path,
        help="path to resulting jsonl file",
    )
    parser.add_argument(
        "-s",
        "--shape",
        metavar="HxW",
        type=lambda x: tuple(map(int, x.split("x"))),
        required=True,
        help="shape of the data to use",
    )
    args = parser.parse_args()
    run(**vars(args))


if __name__ == "__main__":
    main()
