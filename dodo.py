import pathlib

from doit.task import clean_targets
from doit.tools import run_once

DOIT_CONFIG = {
    "action_string_formatting": "new",
    "default_tasks": [
        "train_model",
        "quantize_model",
        "compile_model_for_tpu",
    ],
}


def task_download_datasets():
    import tempfile

    import requests
    import tqdm

    raw_path = pathlib.Path("data/raw/tusimple")

    def download_url(url, target_path):
        target_path.parent.mkdir(parents=True, exist_ok=True)
        response = requests.get(url, stream=True)
        if not response.ok:
            print("Could not download '{}'".format(url))
            return 1
        file_size = response.headers.get("Content-Length", None)
        if file_size:
            file_size = int(file_size)
        temp_target_path = target_path.with_suffix("{}.part".format(target_path.suffix))
        with open(temp_target_path, mode="wb") as target_file, tqdm.tqdm(
            total=file_size, unit="iB", unit_scale=True, unit_divisor=1024
        ) as bar:
            chunk_size = 1024
            for block in response.iter_content(chunk_size):
                size = target_file.write(block)
                bar.update(size)
        temp_target_path.rename(target_path)

    resources = [
        (
            "tusimple_train_set",
            "https://s3.us-east-2.amazonaws.com/benchmark-frontend/datasets/1/train_set.zip",
            raw_path / "train_set.zip",
        ),
        (
            "tusimple_test_set",
            "https://s3.us-east-2.amazonaws.com/benchmark-frontend/datasets/1/test_set.zip",
            raw_path / "test_set.zip",
        ),
        (
            "tusimple_test_baseline",
            "https://s3.us-east-2.amazonaws.com/benchmark-frontend/datasets/1/test_baseline.json",
            raw_path / "test_baseline.json",
        ),
        (
            "tusimple_test_label",
            "https://s3.us-east-2.amazonaws.com/benchmark-frontend/truth/1/test_label.json",
            raw_path / "test_label.json",
        ),
    ]

    for name, url, path in resources:
        yield {
            "name": name,
            "actions": [
                (download_url, [url, path]),
            ],
            "targets": [path],
            "uptodate": [run_once],
        }


def task_extract_archives():
    import tempfile
    import zipfile

    import tqdm

    raw_path = pathlib.Path("data/raw/tusimple")

    def extract_archive(archive_path, target_path):
        with tempfile.TemporaryDirectory() as temp_dir:
            temp_dir = pathlib.Path(temp_dir)
            with zipfile.ZipFile(archive_path, mode="r") as zip_file:
                for file in tqdm.tqdm(
                    iterable=zip_file.namelist(), total=len(zip_file.namelist())
                ):
                    zip_file.extract(member=file, path=temp_dir)
            target_path.mkdir(parents=True, exist_ok=True)
            for file in temp_dir.iterdir():
                file.rename(target_path / file.relative_to(temp_dir))

    resources = [
        ("train_set", raw_path / "train_set.zip", raw_path / "train_set"),
        ("test_set", raw_path / "test_set.zip", raw_path / "test_set"),
    ]

    for name, archive, target in resources:
        yield {
            "name": name,
            "actions": [
                (extract_archive, [archive, target]),
            ],
            "file_dep": [archive],
            "targets": [target],
        }


def task_generate_clip():
    import ffmpeg

    def _generate_files_txt(clips_path, files_txt_path):
        with open(files_txt_path, mode="w") as f:
            for path in clips_path.glob("*/*/"):
                f.write("file 'file:{}'\n".format(str(path / "%d.jpg")))

    for set_type in ["train_set", "test_set"]:
        clips_path = pathlib.Path("data/raw/tusimple/{}/clips".format(set_type))
        files_txt_path = pathlib.Path(
            "data/processed/tusimple/{}/all_files.txt".format(set_type)
        )
        out_path = pathlib.Path(
            "data/processed/tusimple/{}/all_clips.mp4".format(set_type)
        )

        yield {
            "name": set_type,
            "actions": [
                (_generate_files_txt, [clips_path, files_txt_path]),
                [
                    "ffmpeg",
                    "-f",
                    "concat",
                    "-safe",
                    "0",
                    "-r",
                    "20",
                    "-i",
                    files_txt_path,
                    out_path,
                ],
            ],
            "file_dep": [*list(clips_path.glob("**/*.jpg"))],
            "targets": [files_txt_path, out_path],
        }


def task_process_images():
    for set_type in ["train_set", "test_set"]:
        clips_path = pathlib.Path("data/processed/tusimple/{}/clips".format(set_type))
        input_path = pathlib.Path("data/raw/tusimple/{}/clips".format(set_type))
        target = clips_path / "720x1280"
        yield {
            "name": "{}_720x1280".format(set_type),
            "actions": [
                ["mkdir", "-p", clips_path],
                ["ln", "-srTf", input_path, target],
            ],
            "task_dep": ["extract_archives"],
            "file_dep": [*list(input_path.glob("**/20.jpg"))],
            "targets": [target],
        }

        for shape in [(720 // 5, 1280 // 5)]:
            target = clips_path / "{}x{}".format(*shape)
            yield {
                "name": "{}_{}x{}".format(set_type, *shape),
                "actions": [
                    [
                        "resize_images",
                        "-s",
                        "{}x{}".format(*shape),
                        "-o",
                        clips_path,
                        input_path,
                    ],
                ],
                "task_dep": ["extract_archives"],
                "file_dep": [*list(input_path.glob("**/20.jpg"))],
                "targets": [target],
            }


def task_generate_masks():
    set_type = "train_set"
    shape = (144, 256)
    masks_path = pathlib.Path("data/processed/tusimple/{}/masks".format(set_type))
    labels_paths = list(
        pathlib.Path("data/raw/tusimple/{}".format(set_type)).glob("*.json")
    )
    yield {
        "name": "{}_{}x{}".format(set_type, *shape),
        "actions": [
            [
                "generate_masks",
                "-s",
                "{}x{}".format(*shape),
                "-o",
                masks_path,
                *labels_paths,
            ],
        ],
        "task_dep": ["extract_archives"],
        "file_dep": labels_paths,
        "targets": [masks_path / "{}x{}".format(*shape)],
    }

    set_type = "test_set"
    masks_path = pathlib.Path("data/processed/tusimple/{}/masks".format(set_type))
    labels_path = pathlib.Path("data/raw/tusimple/test_label.json")
    yield {
        "name": "{}_{}x{}".format(set_type, *shape),
        "actions": [
            [
                "generate_masks",
                "-s",
                "{}x{}".format(*shape),
                "-o",
                masks_path,
                labels_path,
            ],
        ],
        "task_dep": ["extract_archives"],
        "file_dep": [labels_path],
        "targets": [masks_path / "{}x{}".format(*shape)],
    }


def task_generate_jsonl_records():
    for set_type in ["train_set", "test_set"]:
        for shape in [(720 // 5, 1280 // 5)]:  # , (720, 1280)]:
            shape_str = "{}x{}".format(*shape)
            input_path = pathlib.Path("data/processed/tusimple/{}".format(set_type))
            output_path = pathlib.Path(
                "data/processed/tusimple/{}/{}_records.jsonl".format(
                    set_type, shape_str
                )
            )
            yield {
                "name": "{}_{}x{}".format(set_type, *shape),
                "actions": [
                    [
                        "generate_jsonl_records",
                        "-s",
                        "{}x{}".format(*shape),
                        input_path,
                        output_path,
                    ],
                ],
                "task_dep": ["process_images", "generate_masks"],
                "file_dep": [
                    *list(input_path.glob("**/*.jpg")),
                    *list(input_path.glob("**/*.npy")),
                ],
                "targets": [output_path],
            }


def task_train_model():
    resources = [
        (
            "no_augmentations",
            ["-s", "144x256", "-b", "32"],
            pathlib.Path("data/processed/tusimple/train_set/144x256_records.jsonl"),
            pathlib.Path("models/semlaneseg.tar.gz"),
        ),
        (
            "color_and_quality",
            ["-s", "144x256", "-b", "32", "-a", "color_and_quality"],
            pathlib.Path("data/processed/tusimple/train_set/144x256_records.jsonl"),
            pathlib.Path("models/semlaneseg_color-quality.tar.gz"),
        ),
        (
            "rotation_and_cutout",
            ["-s", "144x256", "-b", "32", "-a", "rotation_and_cutout"],
            pathlib.Path("data/processed/tusimple/train_set/144x256_records.jsonl"),
            pathlib.Path("models/semlaneseg_rotation-cutout.tar.gz"),
        ),
        (
            "color_and_quality+rotation_and_cutout",
            [
                "-s",
                "144x256",
                "-b",
                "32",
                "-a",
                "color_and_quality",
                "-a",
                "rotation_and_cutout",
            ],
            pathlib.Path("data/processed/tusimple/train_set/144x256_records.jsonl"),
            pathlib.Path("models/semlaneseg_color-quality_rotation-cutout.tar.gz"),
        ),
    ]

    for name, args, records_path, save_path in resources:
        yield {
            "name": name,
            "actions": [
                ["train_model", *args, records_path, save_path],
            ],
            "file_dep": [records_path],
            "targets": [save_path],
        }


def task_quantize_model():
    resources = [
        (
            "no_augmentations",
            pathlib.Path("models/semlaneseg.tar.gz"),
            pathlib.Path("models/quantized/semlaneseg.tflite"),
        ),
        (
            "color_and_quality",
            pathlib.Path("models/semlaneseg_color-quality.tar.gz"),
            pathlib.Path("models/quantized/semlaneseg_color-quality.tflite"),
        ),
        (
            "rotation_and_cutout",
            pathlib.Path("models/semlaneseg_rotation-cutout.tar.gz"),
            pathlib.Path("models/quantized/semlaneseg_rotation-cutout.tflite"),
        ),
        (
            "color_and_quality+rotation_and_cutout",
            pathlib.Path("models/semlaneseg_color-quality_rotation-cutout.tar.gz"),
            pathlib.Path(
                "models/quantized/semlaneseg_color-quality_rotation-cutout.tflite"
            ),
        ),
    ]

    for name, model_path, save_path in resources:
        yield {
            "name": name,
            "actions": [
                ["quantize_model", model_path, save_path],
            ],
            "file_dep": [model_path],
            "targets": [save_path],
        }


def task_compile_model_for_tpu():
    out_path = pathlib.Path("models/tpu")

    resources = [
        (
            "no_augmentations",
            pathlib.Path("models/quantized/semlaneseg.tflite"),
            pathlib.Path("models/tpu/semlaneseg_edgetpu"),
        ),
        (
            "color_and_quality",
            pathlib.Path("models/quantized/semlaneseg_color-quality.tflite"),
            pathlib.Path("models/tpu/semlaneseg_color-quality_edgetpu"),
        ),
        (
            "rotation_and_cutout",
            pathlib.Path("models/quantized/semlaneseg_rotation-cutout.tflite"),
            pathlib.Path("models/tpu/semlaneseg_rotation-cutout_edgetpu"),
        ),
        (
            "color_and_quality+rotation_and_cutout",
            pathlib.Path(
                "models/quantized/semlaneseg_color-quality_rotation-cutout.tflite"
            ),
            pathlib.Path("models/tpu/semlaneseg_color-quality_rotation-cutout_edgetpu"),
        ),
    ]

    for name, model_path, target in resources:
        yield {
            "name": name,
            "actions": [
                (out_path.mkdir, [], dict(parents=True, exist_ok=True)),
                ["edgetpu_compiler", "--out_dir", out_path, model_path],
            ],
            "file_dep": [model_path],
            "targets": [target.with_suffix(".tflite"), target.with_suffix(".log")],
        }
