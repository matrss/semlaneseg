# semlaneseg - Semantic Lane Segmentation

Dies ist das git Repository zu meiner Seminararbeit,
welche sich mit der Erkennung und Segmentierung von Fahrbahnmarkierung befasst.

Enthalten sind:
- Der Latex Quellcode der Seminararbeit in `doc/`, welche [hier](https://matrss.gitlab.io/semlaneseg/seminararbeit.pdf) als PDF heruntergeladen werden kann.
- Die Quelldateien der Präsentation in `slides/`, welche [hier](https://matrss.gitlab.io/semlaneseg/slides.html) online zu finden ist.
- Der Quellcode im Python Paket `semlaneseg/`, welcher den gesamten Code zum Erstellen und Trainieren der TensorFlow Modelle beinhaltet.
  Das Paket stellt einzelne Schritte,
  wie die Vorverarbeitung von Datensätzen oder das Training selbst,
  als Skripte zur Verfügung.


## Installation

Um eine Python Umgebung mit allen Abhängigkeiten des Pakets zu erhalten benutzen wir [poetry](https://python-poetry.org/):

```{sh}
poetry install
```

Für das Kompilieren für die Coral Edge TPU bzw. die Ausführung der Modelle auf der TPU benötigen wir die entsprechende Library und den Compiler.
Da Coral ihre Pakete nur für Debian und Derivate zur Verfügung stellt sind diese Distributions-spezifisch:

```{sh}
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
sudo apt-get update
sudo apt-get install libedgetpu1-max edgetpu-compiler
```

Um das Training von TensorFlow Modellen zu beschleunigen kann optional GPU Support via CUDA genutzt werden.
Hierzu verweise ich auf die [TensorFlow Dokumentation](https://www.tensorflow.org/install/gpu).


## Anwendung

### Inferenz für Videodateien

Die Inferenz ist für Videodateien mit dem Skript `infer` möglich.
Ein beispielhafter Befehl für eine Inferenz auf der TPU kann wie folgt aussehen:

```{sh}
poetry run infer -s 144x256 -t tflite-tpu -m models/tpu/semlaneseg_edgetpu.tflite example_data/clips.mp4
```

Wobei die Option `-s` die Auflösung des Videos in `Hoehe x Breite` bezeichnet.
Das Skript simuliert das Einlesen der Videodatei mit `ffmpeg` entsprechend der framerate des Videos,
sodass dies vergleichbar mit einem Live-Video einer Kamera wird.

Die Option `-t` gibt an,
um was für eine Art Modell es sich handelt.
Mit dem Wert `tflite` können nicht-TPU TensorFlow Lite Modelle genutzt werden,
mit dem Wert `tf` auch die normalen TensorFlow Modelle.
Um obige Inferenz also mit dem TensorFlow Modell durchzuführen kann folgendes genutzt werden:

```{sh}
poetry run infer -s 144x256 -t tf -m models/semlaneseg.tar.gz example_data/clips.mp4
```

Neben der Segmentierung implementiert das Skript auch eine einfache Heuristik zur Anwendung als Spurhaltewarnsystem,
welches in der am Anfang verlinkten Ausarbeitung näher erläutert wird.
Dieses benötigt zusätzlich jeweils eine Gerade,
welche die erwartete Position der linken und rechten Spurbegrenzung der aktuellen Fahrspur approximieren.
Diese Geraden werden durch zwei Punkte im Bild definiert,
welche als Kommandozeilenargumente übergeben werden.
Für den Beispielclip in `example_data/clips.mp4` sähe ein Aufruf des Skript dann zum Beispiel wie folgt aus:

```{sh}
poetry run infer -s 144x256 -t tflite-tpu -m models/tpu/semlaneseg_color-quality_rotation-cutout_edgetpu.tflite --stay_in_lane --left_lane_points "0,128;211,0" --right_lane_points "24,0;256,131" example_data/clips.mp4
```

Das Skript enthält noch weitere Optionen,
welche zum Beispiel das Speichern der Ausgabe in einer Videodatei ermöglichen.
Mehr Infos gibt es auch mit `--help`.


### Eigenes Training

Der gesamte Prozess vom Download des Datensatz über das Training der Modelle bis zum Quantisieren und Kompilieren für die Coral Edge TPU ist mit Hilfe von [doit](https://pydoit.org/) automatisiert.
Um einen Trainingsprozess anzustossen reicht der Befehl

```{sh}
poetry run doit
```

Dies führt doit in der zuvor installierten Python Umgebung aus.
Gegebenenfalls muss zuvor das `models/` Verzeichnis gelöscht werden.
Achtung: dies führt dazu, dass unter dem Namen `data/` ein Verzeichnis mit einer Größe von etwa 45GB angelegt wird.
