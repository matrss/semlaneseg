---
title: Semantische Segmentierung zur Identifikation von Längsmarkierungen im Straßenverkehr
author: Matthias Riße
date: 21. Januar 2022
---


# Ziele

- Erkennung von Längsmarkierungen im Straßenverkehr
  - Mittels Deep Learning und semantischer Segmentierung
  - Ausführung auf der Coral Edge TPU
- Transfer zur Segmentierung eigener Aufnahmen
<!--
- Evaluierung verschiedener Image Augmentations
-->
- Anwendung als (passiver) "Spurhalteassistent"

::: notes
- Längsmarkierungen:
  - Alle Straßenmarkierungen parallel zur Fahrtrichtung
  - Bspw. Leitlinie, Warnlinien, Fahrbahnbegrenzung, Fahrstreifenbegrenzung
- Evaluierung verschiedener Image Augmentations
  - Motiviert durch unterschiedliche Kameraperspektive zwischen Training und eigenen Aufnahmen
  - Farblich unterschiedliche Markierung
:::


# Semantische Segmentierung

- Bild &rarr; Bild
- Klassifikation jedes einzelnen Pixels
- Ergebnis: Segmentierungsmaske

<figure>
![](static/graphics/cat.png){ width=45% }
![](static/graphics/cat_segmented.png){ width=45% }
<figcaption>Quelle: <https://cs231n.stanford.edu/slides/2017/cs231n_2017_lecture11.pdf></figcaption>
</figure>

::: notes
- Abbildung: Bild zu Bild
- Klassifikationsproblem für jedes einzelne Pixel
  - In semantischer Segmentierung oft: gehört Pixel zu Klasse A, B oder C
- In diesem Fall Unterscheidung zur "instance segmentation" etwas schwammig
  - Statische Anzahl an zu segmentierenden Klassen
  - In instance segmentation eher dynamische Anzahl an Objekten pro Klasse
:::


# Datensatz

- Bereitgestellt von TuSimple
- "Lane Detection Challenge" zur CVPR 2017
- Aufnahmen aus fahrenden Fahrzeugen auf Highways in den USA
- Über 6000 Datenpunkte

:::: columns
::: { .column width=31% }
![](static/graphics/0313-1_28920_20_image.jpg){ width=100% }
![](static/graphics/0313-1_28920_20_mask.png){ width=100% }
:::
::: { .column width=50% }

```{ .small }
{
  "lanes": [
    [-2, -2, -2, -2, 637, 628, 619, 610, 601, 591, 582, 573, 564, 555, 546, 537, 528, 518, 509, 500, 491, 482, 473, 464, 454, 445, 436, 427, 418, 409, 400, 391, 381, 372, 363, 354, 345, 336, 327, 317, 308, 299, 290, 281, 272, 263, 254, -2],
    [-2, -2, -2, -2, 702, 714, 725, 737, 749, 760, 772, 784, 795, 807, 818, 830, 842, 853, 865, 877, 888, 900, 911, 923, 935, 946, 958, 970, 981, 993, 1004, 1016, 1028, 1039, 1051, 1062, 1074, 1086, 1097, 1109, 1121, 1132, 1144, 1155, 1167, 1179, 1190, 1202],
    [-2, -2, -2, -2, 577, 546, 516, 485, 455, 424, 394, 363, 333, 302, 272, 242, 211, 181, 150, 120, 89, 59, 28, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2],
    [-2, -2, -2, -2, 782, 816, 850, 883, 917, 951, 984, 1018, 1051, 1085, 1119, 1152, 1186, 1220, 1253, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2, -2]
  ],
  "h_samples": [240, 250, 260, 270, 280, 290, 300, 310, 320, 330, 340, 350, 360, 370, 380, 390, 400, 410, 420, 430, 440, 450, 460, 470, 480, 490, 500, 510, 520, 530, 540, 550, 560, 570, 580, 590, 600, 610, 620, 630, 640, 650, 660, 670, 680, 690, 700, 710],
  "raw_file": "clips/0313-1/28920/20.jpg"
}
```

:::
::::

::: notes
- Von TuSimple
  - Firma aus den USA
  - Autonomous Trucking
- CVPR: Conference on Computer Vision and Pattern Recognition
- Trainingsdaten: 3626
  - Split in: 80% Trainingsdaten, 20% Validierungsdaten
- Testdaten: 2782
- Je Datenpunkt eine Bildfolge von 20 Bildern, welche einen 1s langen Clip darstellen
  - Bilder mit Auflösung 1280x720
    - Runterskaliert auf 256x144
- Segmentierung für das letzte Bild, als variable Anzahl an Polygonzügen
- Einheitliche Rasterung in y-Koordinate
- Segmentierungsmaske generiert aus den Polygonzügen
  - In 144x256 Bild eingezeichnet
:::


# Modellarchitektur

![](static/graphics/generated/model.svg){ height=100% }

::: notes
- Orientiert an U-Net
  - Paper aus 2015
  - Anwendungsbereich ursprünglich im Biomedizin Bereich
- Eingabebilder: {144x256x3} &rarr; {144x256x7}
- Zwei Teile:
  - Kontraktionsteil
  - Expansionsteil
- Kontraktionsteil:
  - Verdopplung der Featuredimension in jeder Schicht
  - Halbierung der räumlichen Dimensionen
- Expansionsteil:
  - Invers zum Kontraktionsteil
  - Conv2dTranspose (up-conv) um räumliche Dimension zu erhöhen
  - "skip" Verbindungen von Convolutions des Kontraktionsteils
    - Kombinieren "globalere" und "lokalere" Features
- zum Schluss 1x1 Convolution auf Anzahl Klassen
:::


# TensorFlow Lite & Coral Edge TPU

:::: .columns
::: { .column width=40% }
- TensorFlow Lite:
  - Framework zur Inferenz mit neuronalen Netzen
  - Speziell IoT, Edge und Mobile Anwendungen
- Coral Edge TPU:
  - "Delegate" für TensorFlow Lite &rarr; Co-Prozessor
  - Spezialisiert auf 8-bit Integer Arithmetik
  - Effizient: 4 TOPS bei 2 Watt
:::
::: { .column width=50% }
![Quelle: <https://coral.ai/docs/edgetpu/models-intro#compatibility-overview>](static/graphics/tpu_compile_workflow.png){ width=100% }

![Quelle: <https://blog.tensorflow.org/2020/04/quantization-aware-training-with-tensorflow-model-optimization-toolkit.html>](static/graphics/quant_image.png){ width=100% }
:::
::::

::: notes
- TensorFlow Lite:
  - Inferenz
  - IoT, Edge, Mobile
  - Delegates zur Beschleunigung: GPU, DSP, NNAPI
- Coral TPU:
  - Ein möglicher Delegate
  - Spezialisiert auf 8-bit Integer Arithmetik
    - bzw. in Zwischenschritten mehr
  - In verschiedenen Formfaktoren erhältlich (USB, M.2 u.a.)

1. Erstelle Modell (Training)
2. Quantisieren mit repräsentativem Datensatz
3. Kompiliere zu TPU kompatiblem Modell

- Kompatibilität eingeschränkt:
  - FullyConnected Layer
  - Conv2D, aber nicht 3D
  - LSTM nur unidirektional
:::


# Ergebnisse


## Metriken

:::: .columns
::: { .column width=24% }
![Loss](static/plots/semlaneseg_color-quality_rotation-cutout-Loss,Curve-training_loss,Curve-validation_loss.svg){ width=100% }
:::
::: { .column width=24% }
![IoU Trainingsdaten](static/plots/semlaneseg_color-quality_rotation-cutout-Training_IoU,Curve-0,Curve-1,Curve-2,Curve-...-5,Curve-6.svg){ width=100% }
:::
::: { .column width=24% }
![IoU Validierungsdaten](static/plots/semlaneseg_color-quality_rotation-cutout-Validation_IoU,Curve-0,Curve-1,Curve-2,Curv...-5,Curve-6.svg){ width=100% }
:::
::: { .column width=24% }
![Confusion Matrix](static/plots/semlaneseg_color-quality_rotation-cutout-Confusion_Matrix.svg){ width=100% }
:::
::::

::: notes
- Trainiert mit Cross Entropy Loss
- Mit Augmentations:
  - Mehr Epochen
  - Loss nahe beieinander
- Intersection over Union Scores:
  - Schnittmenge durch Vereinigung
  - Pro segmentierter Klasse
- Confusion Matrizen: relativ gute Ergebnisse für Klassen 1 bis 4, größter Fehler ist falsche Klassifikation in Klasse 0
  - Klasse 5: in Validierungsdaten nicht vorhanden
:::


## GPU &rarr; TPU

:::: columns
::: { .column width=40% }
![GPU](static/plots/semlaneseg_color-quality_rotation-cutout-Confusion_Matrix.svg){ width=100% }
:::
::: { .column width=40% }
![TPU](static/plots/semlaneseg_color-quality_rotation-cutout-tpu-Confusion_Matrix.svg){ width=100% }
:::
::::

::: notes
- Sehr ähnliche Scores
:::


## Testset

:::: columns
::: { .column width=40% }
![Validierungsdaten](static/plots/semlaneseg_color-quality_rotation-cutout-tpu-Confusion_Matrix.svg){ width=100% }
:::
::: { .column width=40% }
![Testdaten](static/plots/semlaneseg_color-quality_rotation-cutout-tpu-test-Confusion_Matrix.svg){ width=100% }
:::
::::

::: notes
- Performance ähnlich zu Validierungsdaten &rarr; gute Generalisierung
:::


## Segmentierungen

![](static/plots/semlaneseg_color-quality_rotation-cutout,RGB-Beispielbild,RGB-Beispielmaske,Image-Ko...mentierung.svg){ width=100% }

:::: columns
::: { .column width=45% }
![](static/plots/semlaneseg_color-quality_rotation-cutout-own_2-tpu.svg){ width=100% }
:::
::: { .column width=45% }
![](static/plots/semlaneseg_color-quality_rotation-cutout-own_4-tpu.svg){ width=100% }
:::
::::

# Anwendung

Spurhalteheuristik:

- Sei $A = \{P_1, \ldots, P_n\}$ mit $P_i = (x_i, y_i), \quad y_i \neq y_j \quad \forall_{j \in \{1, \ldots, n\}}$ eine Menge von Punkten, die eine segmentierte Fahrbahnbegrenzung darstellt
- Sei $y = ax + b \iff x = \frac{y-b}{a}$ eine Gerade, welche den erwarteten Verlauf der Markierung darstellt
- Bestimme mittlere Distanz zum erwarteten Verlauf: $d = \frac{1}{n} \sum_{(x, y) \in A} \frac{(y-b)/a - x}{256}$
- Bestimme obige Distanz für linke und rechte Fahrbahnbegrenzung: $d_1, d_2$
- Bestimme Mittelwert der Beträge: $c = \frac{\lvert d_1 \rvert + \lvert d_2 \rvert}{2}$
- Wenn $c$ größer als ein Schwellwert $0.05$:
  - Bestimme Vorzeichen des betragsmäßig größeren $d_k$
    - Negativ &rarr; Abweichung nach links
    - Positiv &rarr; Abweichung nach rechts

::: notes
- Erwarteter Verlauf: bei Ausführung angegeben, approximative Grade für Markierung
:::


## Anwendung

:::: columns
::: { .column width=45% }
![Clip aus den Testdaten](static/example_data/test_clips_segmented.mp4){ width=100% }
:::
::: { .column width=45% }
![Eigene Aufnahmen](static/example_data/clips_segmented.mp4){ width=100% }
:::
::::


# Fazit

- Semantische Segmentierung von Fahrbahnmarkierungen möglich
  - Auch in Echtzeit auf der Coral Edge TPU
<!--- Image Augmentations können Training stabilisieren und Generalisierung verbessern-->
- Kein nennenswerter Qualitätsverlust durch Quantisierung

Mögliche Modifikationen:

- Andere Zielmenge &rarr; generiere Polygonzüge
- Klassifikation der Art der Markierungen

::: notes
- Andere Parameter:
  - Auflösung
  - Hyperparameter des Modells
  - Parameter der Image Augmentations
- Andere Ausgaben:
  - Segmentierung von Fahrbahnmarkierungen allgemein, anschließend Clustern
  - Erzeuge Polygonzüge aus Eingabebildern
:::


# Danke für die Aufmerksamkeit! {- .unlisted}


# Image Augmentations

- HSV Farbwertänderungen & Kontrast
  - Invarianz gegen unterschiedliche Farben von Markierungen
  - Invarianz gegen unterschiedliche Lichtverhältnisse
- JPEG Kompressionsänderungen
- 3-dimensionale Rotationen
  - Invarianz gegen leicht unterschiedliche Kameraausrichtungen
- Cutouts
  - Invarianz gegen Verdeckungen im Sichtfeld

::: notes
- Alle Augmentations jeweils mit zufälligen Parametern
:::


# Segmentierungen der Trainingsdaten

![Ohne Augmentations](static/plots/semlaneseg,RGB-Beispielbild,RGB-Beispielmaske,Image-Konfidenz,RGB-Segmentierung.svg){ width=100% }

![Farbe, JPEG Qualität, Rotation und Cutout](static/plots/semlaneseg_color-quality_rotation-cutout,RGB-Beispielbild,RGB-Beispielmaske,Image-Ko...mentierung.svg){ width=100% }


# Metriken

<figure>
![](static/plots/semlaneseg-Loss,Curve-training_loss,Curve-validation_loss.svg){ width=24% }
![](static/plots/semlaneseg-Training_IoU,Curve-0,Curve-1,Curve-2,Curve-3,Curve-4,Curve-5,Curve-6.svg){ width=24% }
![](static/plots/semlaneseg-Validation_IoU,Curve-0,Curve-1,Curve-2,Curve-3,Curve-4,Curve-5,Curve-6.svg){ width=24% }
![](static/plots/semlaneseg-Confusion_Matrix.svg){ width=24% }
<figcaption>Ohne Augmentations: Loss, IoU Trainingsdaten, IoU Validierungsdaten, Confusion Matrix</figcaption>
</figure>

<figure>
![](static/plots/semlaneseg_color-quality_rotation-cutout-Loss,Curve-training_loss,Curve-validation_loss.svg){ width=24% }
![](static/plots/semlaneseg_color-quality_rotation-cutout-Training_IoU,Curve-0,Curve-1,Curve-2,Curve-...-5,Curve-6.svg){ width=24% }
![](static/plots/semlaneseg_color-quality_rotation-cutout-Validation_IoU,Curve-0,Curve-1,Curve-2,Curv...-5,Curve-6.svg){ width=24% }
![](static/plots/semlaneseg_color-quality_rotation-cutout-Confusion_Matrix.svg){ width=24% }
<figcaption>Farbe, JPEG Qualität, Rotation und Cutout: Loss, IoU Trainingsdaten, IoU Validierungsdaten, Confusion Matrix</figcaption>
</figure>

::: notes
- Trainiert mit Cross Entropy Loss
- Ohne Augmentations:
  - Weniger Epochen
  - Verhältnismäßig große Differenz zwischen Trainings- und Validierungsloss
- Mit Augmentations:
  - Mehr Epochen
  - Loss nahe beieinander
- Intersection over Union Scores:
  - Schnittmenge durch Vereinigung
  - Pro segmentierter Klasse
- Trainings IoU geht durch Augmentations etwas zurück
- Validierungs IoU durch Augmentations etwas stabiler und näher an Trainings IoU
- Confusion Matrizen: relativ gute Ergebnisse für Klassen 1 bis 4, größter Fehler ist falsche Klassifikation in Klasse 0
  - Klasse 5: in Validierungsdaten nicht vorhanden
:::


# Transfer auf eigene Aufnahmen

:::: columns
::: { .column width=45% }
<figure>
![](static/plots/semlaneseg-own_2-tpu.svg){ width=100% }
![](static/plots/semlaneseg-own_4-tpu.svg){ width=100% }
<figcaption>Ohne Augmentations</figcaption>
</figure>
:::
::: { .column width=45% }
<figure>
![](static/plots/semlaneseg_color-quality_rotation-cutout-own_2-tpu.svg){ width=100% }
![](static/plots/semlaneseg_color-quality_rotation-cutout-own_4-tpu.svg){ width=100% }
<figcaption>Farbe, JPEG Qualität, Rotation und Cutout</figcaption>
</figure>
:::
::::

::: notes
- Scheinbar bessere Segmentierung für Klassen 1 und 2 (rot und grün) mit Image Augmentations
- Andere Klassen werden mit Image Augmentations schlechter
  - Blau fehlt im zweiten Bild rechts komplett
- Gute Segmentierung der Klassen 1 und 2
- Klassen 3, 4, 5 und 6 quasi unbrauchbar
:::
